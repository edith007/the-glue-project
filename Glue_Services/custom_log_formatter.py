from logging import Formatter
from datetime import datetime

class CustomLogFormatter(Formatter):
    def format(self, record):
        now = datetime.now().strftime('%d/%b/%Y:%H:%M:%S +0000')
        message = super(CustomLogFormatter, self).format(record)

        # Use getattr to access the custom attributes, and provide default values
        remote_addr = getattr(record, 'remote_addr', '-')
        request_method = getattr(record, 'request_method', '-')
        path = getattr(record, 'path', '-')
        status_code = getattr(record, 'status_code', '-')

        log_message = f'[{now}] {remote_addr} - - {request_method} {path} {status_code} {message}'
        return log_message
