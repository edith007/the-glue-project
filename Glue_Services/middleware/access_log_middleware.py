import logging
from datetime import datetime

logger = logging.getLogger(__name__)

class AccessLogMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        
        client_ip = request.META.get('REMOTE_ADDR', '-')
        method = request.method
        path = request.path
        status_code = response.status_code
        user_agent = request.META.get('HTTP_USER_AGENT', '-')
        extra_info = 'extra information here' # Add extra details as needed

        log_message = f'[{datetime.now().strftime("%d/%b/%Y:%H:%M:%S %z")}] {client_ip} - - {method} {path} 789 {status_code} 611 89 - "{user_agent}" {extra_info}'
        
        logger.info(log_message)

        return response
