# Glue Services

Glue Services is a Django-based project designed to create and manage dynamic forms, questions, and responses. It provides an interface for submitting responses, viewing details, and offering a set of post-processing tasks to be executed asynchronously once a form response is submitted.

## Features

- **Form Management:** Create and update forms with an easy-to-use interface, and add questions to forms dynamically.
- **Response Processing:** Asynchronously process responses using a task queue. Form responses trigger a post_save signal, publishing tasks to a RabbitMQ queue. Celery worker processes, acting as subscribers, consume and process these tasks.
- **Metrics Collection:** Integrated Prometheus and Prometheus Node Exporter for system and application metrics collection. This includes system-level metrics like CPU usage, memory usage, disk IO, network traffic, as well as application-level metrics like request rate, error rate, response times, etc.
- **Logging:** Integrated Django's built-in logging framework to improve observation on logs. The log entries have been enhanced with timestamp, date, module, and message.
- **Rate Limiting & Retry Logic:** Implemented rate limiting and retry logic in Google Sheets task execution. We have incorporated a retry mechanism to manage API rate limit exceedances.
- **Worksheet Management:** Implemented multi-task and form-wise worksheet creation. We ensured that a new worksheet is created for each unique form and responses are added to the correct worksheet.
- **Extendable Post-Processing Tasks:** Introduced extendable post-processing task functionality. Enhanced the Form model to associate multiple post-processing tasks using a ManyToMany field. Also introduced a sample task, 'Add SMS Plugin'.
- **Docker Integration:** Implemented full Docker integration for application containerization.
- **Asynchronous Processing:** Introduced asynchronous processing of form responses using Celery and RabbitMQ.

## Integrations

- Prometheus Node Exporter for real-time system metrics
- Google Sheets for data aggregation and visualization
- Celery for asynchronous task processing
- RabbitMQ as the message broker for task queue management
- Docker for containerized development and deployment

## Tech Stack

- Python/Django for backend development
- PostgreSQL as the primary data store
- Celery for asynchronous task processing
- RabbitMQ as the message broker
- Docker for application containerization
- Prometheus Node Exporter for system monitoring

[Project Running Instructions]

## Getting Started

Instructions for setting up and running the project:

1. Make sure Docker and Docker Compose are installed on your machine.
2. Build and run the Docker containers using the command `docker-compose up`.
3. Visit `http://0.0.0.0:8000/` to access the Glue Services application.
4. Visit `http://localhost:9090/graph` to access the Prometheus metrics dashboard.

## Strategy and Design

The project follows a Publisher-Subscriber architecture, where the form submission acts as the publisher. Each form response triggers a post_save signal, publishing tasks to a RabbitMQ queue. Celery worker processes, acting as subscribers, consume and process these tasks asynchronously.

## Challenges vs Hacks

One of the primary challenges we faced was the need to aggregate all answers related to a response in a single call. Our initial model structure was triggering a post_save signal for every answer, resulting in multiple signals for each response. To overcome this, we implemented a significant change in our data models - incorporating a `JSONField` in the `Response` model to store all associated answers as a JSON object. This approach simplified the signal handling and allowed easier retrieval of all answers related to a response.

## Links

- Google Drive link to Demo : https://drive.google.com/file/d/1ipDktwPaR7UU4yHtQj31nk2CxwVEziJo/view?usp=sharing

- Goole Sheet link : https://docs.google.com/spreadsheets/d/1jg1WGiZGPHkjN7vaWyZhzrICptBBc1bcLwkqfWgHmUg/edit?usp=sharing

- Prometheus : http://localhost:9090/graph?g0.expr=node_cpu_seconds_total&g0.tab=0&g0.stacked=1&g0.show_exemplars=0&g0.range_input=1h&g1.expr=node_memory_MemTotal_bytes&g1.tab=0&g1.stacked=1&g1.show_exemplars=0&g1.range_input=1h&g2.expr=node_disk_io_now&g2.tab=0&g2.stacked=1&g2.show_exemplars=0&g2.range_input=1h