from django.contrib import admin
from .models import Form, Question, Response

# Register your models here.
models = [Form, Question, Response]

for model in models:
    admin.site.register(model)
