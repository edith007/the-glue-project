from django.urls import path
from . import views

app_name = 'ResponsePostProcessor'

urlpatterns = [
    path('', views.home, name='home'),
    path('form/new/', views.FormCreateView.as_view(), name='form_new'),
    path('form/<int:pk>/', views.FormDetailView.as_view(), name='form_detail'),
    path('form/<int:pk>/edit/', views.FormUpdateView.as_view(), name='form_edit'),
    path('form/<int:pk>/delete/', views.FormDeleteView.as_view(), name='form_delete'),
    path('form/<int:form_pk>/question/new/', views.QuestionCreateView.as_view(), name='question_new'),
    path('form/<int:form_pk>/respond/', views.FormResponseCreateView.as_view(), name='form_respond'),
    path('form/<int:form_pk>/response/<int:pk>/', views.ResponseDetailView.as_view(), name='response_detail'),
    path('tasks/', views.PostProcessingTaskListView.as_view(), name='task_list'),
    path('tasks/create/', views.PostProcessingTaskCreateView.as_view(), name='task_create'),
]