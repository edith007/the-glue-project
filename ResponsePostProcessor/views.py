from .models import Form, Question, Response, PostProcessingTask
from .forms import FormCreateForm, QuestionForm, DynamicForm, PostProcessingTaskForm
from django.urls import reverse
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic import View
from django.core.exceptions import ObjectDoesNotExist

# Create your views here.
def home(request):
    return render(request, 'ResponsePostProcessor/home.html')

class FormCreateView(CreateView):
    model = Form
    fields = ['title', 'description', 'post_processing_tasks']
    template_name = 'ResponsePostProcessor/form_edit.html'

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def get_success_url(self):
        return reverse('ResponsePostProcessor:form_detail', kwargs={'pk': self.object.pk})

class FormDetailView(DetailView):
    model = Form
    template_name = 'ResponsePostProcessor/form_detail.html'

class FormUpdateView(UpdateView):
    model = Form
    fields = ['title', 'description']
    template_name = 'ResponsePostProcessor/form_edit.html'

    def form_valid(self, form):
        return super().form_valid(form)

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def get_success_url(self):
        return reverse('ResponsePostProcessor:form_detail', kwargs={'pk': self.object.pk})

class FormDeleteView(DeleteView):
    model = Form
    template_name = 'ResponsePostProcessor/form_confirm_delete.html'
    context_object_name = 'form'

    def get_success_url(self):
        return reverse('ResponsePostProcessor:home')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_id'] = self.kwargs['pk']
        return context

class QuestionCreateView(CreateView):
    model = Question
    form_class = QuestionForm
    template_name = 'ResponsePostProcessor/question_edit.html'

    def form_valid(self, form):
        form.instance.form = Form.objects.get(pk=self.kwargs['form_pk'])
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('ResponsePostProcessor:form_detail', kwargs={'pk': self.kwargs['form_pk']})

class FormResponseCreateView(View):
    template_name = 'ResponsePostProcessor/form_respond.html'

    def get(self, request, *args, **kwargs):
        form = Form.objects.get(id=kwargs.get('form_pk'))
        questions = form.questions.all()
        response_form = DynamicForm(questions=questions)
        return render(request, self.template_name, {'form': form, 'response_form': response_form})

    def post(self, request, *args, **kwargs):
        print("COMING TO POST FUNCTION AFTER A POST SUBMISSION")
        try:
            form = Form.objects.get(id=kwargs.get('form_pk'))
            questions = form.questions.all()
            response_form = DynamicForm(request.POST, questions=questions)

            if response_form.is_valid():
                response = Response.objects.create(form=form, answers=response_form.cleaned_data)
                return redirect('ResponsePostProcessor:response_detail', form_pk=form.pk, pk=response.pk)

            return render(request, self.template_name, {'form': form, 'response_form': response_form})
        except ObjectDoesNotExist as e:
            return render(request, 'ResponsePostProcessor/error.html', {'message': 'The requested form does not exist.'})
        except Exception as e:
            return render(request, 'ResponsePostProcessor/error.html', {'message': 'An error occurred while processing your request.'})

class ResponseDetailView(DetailView):
    model = Response
    template_name = 'ResponsePostProcessor/response_detail.html'
    context_object_name = 'response'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = self.object.form
        return context

class PostProcessingTaskListView(ListView):
    model = PostProcessingTask
    template_name = 'ResponsePostProcessor/post_processing_task_list.html'
    context_object_name = 'tasks'

class PostProcessingTaskCreateView(CreateView):
    model = PostProcessingTask
    form_class = PostProcessingTaskForm
    template_name = 'ResponsePostProcessor/post_processing_task_create.html'

    def get_success_url(self):
        return reverse('ResponsePostProcessor:task_list')