from django import forms
from .models import Form, Question, PostProcessingTask

class FormCreateForm(forms.ModelForm):
    class Meta:
        model = Form
        fields = ['title', 'description', 'post_processing_tasks']
        widgets = {
            'post_processing_tasks': forms.CheckboxSelectMultiple,
        }

    def clean_title(self):
        title = self.cleaned_data.get('title')
        if not title:
            raise forms.ValidationError('The title cannot be empty.')
        return title

class PostProcessingTaskForm(forms.ModelForm):
    class Meta:
        model = PostProcessingTask
        fields = ['name', 'description']

class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ['text', 'question_type']

class DynamicForm(forms.Form):
    def __init__(self, *args, **kwargs):
        questions = kwargs.pop('questions')
        super(DynamicForm, self).__init__(*args, **kwargs)
        for question in questions:
            self.fields[f'question_{question.id}'] = forms.CharField(label=question.text)