from django.db import models

# Create your models here

class PostProcessingTask(models.Model):
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class Form(models.Model):
    GOOGLE_SHEETS = 'GS'
    POST_PROCESS_CHOICES = [
        (GOOGLE_SHEETS, 'Google Sheets'),
        # Add other post-processing tasks here as needed
    ]
    title = models.CharField(max_length=200)
    description = models.TextField()
    post_processing_tasks = models.ManyToManyField(PostProcessingTask, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


class Question(models.Model):
    MULTIPLE_CHOICE = 'MC'
    TEXT = 'TXT'
    QUESTION_TYPE_CHOICES = [
        (MULTIPLE_CHOICE, 'Multiple Choice'),
        (TEXT, 'Text'),
    ]
    form = models.ForeignKey(Form, related_name='questions', on_delete=models.CASCADE)
    text = models.TextField()
    question_type = models.CharField(max_length=3, choices=QUESTION_TYPE_CHOICES, default=TEXT)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.text


class Response(models.Model):
    form = models.ForeignKey(Form, related_name='responses', on_delete=models.CASCADE)
    # New field to hold all answers as a JSON object
    answers = models.JSONField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def get_response_data(self):
        response_data = {}
        for question_id, answer in self.answers.items():
            # Split the string by underscore and take the second part as the id
            numeric_id = question_id.split('_')[1]
            question = Question.objects.get(id=numeric_id)
            response_data[question.text] = answer
        return response_data
