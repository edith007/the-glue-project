from .tasks_classes.google_sheets_task import GoogleSheetsTask
from .tasks_classes.add_sms_plugin import AddSMSPlugin

TASK_REGISTRY = {
    'google_sheets': GoogleSheetsTask,
    'Add SMS Plugin': AddSMSPlugin,
}
