from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import Response
from .tasks import process_form_response
from .task_registry import TASK_REGISTRY

@receiver(post_save, sender=Response)
def post_save_response(sender, instance, created, **kwargs):
    if created:
        try:
            response_data = instance.get_response_data()
            form = instance.form
            response_data["form_name"] = form.title
            for task in form.post_processing_tasks.all():
                process_form_response.delay(response_data, task.name)
        except Exception as e:
            print(f"An error occurred while processing form response: {e}")
