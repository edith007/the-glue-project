import os
import time
import logging
from .base_task import BaseTask
import gspread
from oauth2client.service_account import ServiceAccountCredentials

# Set up logging
logger = logging.getLogger(__name__)

class GoogleSheetsTask(BaseTask):
    MAX_RETRIES = 5  # specify maximum retry attempts
    INITIAL_WAIT_TIME = 5 * 60  # initial wait time in seconds (5 minutes)

    def execute(self, response_data):
        print("FROM G-SHEET TASK!")
        print(f"response data = {response_data}")
        scope = ["https://spreadsheets.google.com/feeds",
                 "https://www.googleapis.com/auth/spreadsheets",
                 "https://www.googleapis.com/auth/drive.file",
                 "https://www.googleapis.com/auth/drive"]
        creds_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), "Cred.json")
        creds = ServiceAccountCredentials.from_json_keyfile_name(creds_file, scope)
        client = gspread.authorize(creds)
        spreadsheet = client.open("Glue Services")

        # pop the form name from response_data; use "Untitled Form" as a default if form_name isn't present
        form_name = response_data.pop('form_name', "Untitled Form")

        try:
            sheet = spreadsheet.worksheet(form_name)
        except gspread.WorksheetNotFound:
            sheet = spreadsheet.add_worksheet(title=form_name, rows="100", cols="20")
            sheet.append_row(list(response_data.keys()))

        for attempt in range(self.MAX_RETRIES):
            try:
                # Append row data to the sheet
                row_data = list(response_data.values())
                sheet.append_row(row_data)
                break  # if successful, we break the retry loop
            except gspread.exceptions.APIError as error:
                if error.response.status_code == 429:
                    # if we get rate limit exceeded error, we wait and then continue the loop to retry
                    wait_time = self.INITIAL_WAIT_TIME * attempt  # updated formula from doc!
                    time.sleep(wait_time)
                else:
                    raise
        else:
            logger.error(f"Maximum retry attempts exceeded for form: {form_name}")
            raise Exception(f"Maximum retry attempts exceeded for form: {form_name}")
