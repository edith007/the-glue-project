class BaseTask:
    def execute(self, response_data):
        raise NotImplementedError
