from __future__ import absolute_import, unicode_literals
from celery import shared_task
from .task_registry import TASK_REGISTRY

@shared_task
def process_form_response(response_data, task_type):
    print("=====FROM TASKS.PY=========")
    try:
        print(f"task type = {task_type}")
        task_class = TASK_REGISTRY.get(task_type)
        if task_class is None:
            raise Exception(f"No task found for type {task_type}")
        task_instance = task_class()
        task_instance.execute(response_data)
    except Exception as e:
        print(f"An error occurred while processing form response: {e}")
        raise e
