from django.apps import AppConfig


class ResponsepostprocessorConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ResponsePostProcessor'

    def ready(self):
        import ResponsePostProcessor.signals